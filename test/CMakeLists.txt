set(TARGET_NAME test)
set(TARGET_SOURCES src/test.cpp)
set(TARGET_DEPS heap)

add_executable(${TARGET_NAME} ${TARGET_SOURCES})
target_link_libraries(${TARGET_NAME} ${TARGET_DEPS})
