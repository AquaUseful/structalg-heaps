#include <array>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <random>

#include "Heap.hpp"

int main() {
  std::array<int, 10> a{};
  heap::Heap h{a.begin(), a.end()};

  std::random_device rd{};
  std::mt19937_64 rgen{rd()};
  std::uniform_int_distribution<int> dist{-100, 100};

  int i = 1;
  while (!h.is_full()) {
    h.insert(i += 2);
  }

  for (int i = 3; i < h.max(); i += 2) {
    h.replace(i, i + 1);
  }

  while (!h.is_empty()) {
    std::cout << h.max() << ' ';
    h.pop();
  }
  std::cout << '\n';

  return EXIT_SUCCESS;
}