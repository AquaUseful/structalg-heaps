#pragma once

#include <algorithm>
#include <bits/iterator_concepts.h>
#include <concepts>
#include <cstddef>
#include <functional>
#include <iterator>

namespace heap {
template <
    std::random_access_iterator It,
    std::predicate<typename std::iterator_traits<It>::value_type,
                   typename std::iterator_traits<It>::value_type>
        Comp = std::less_equal<typename std::iterator_traits<It>::value_type>>
  requires std::totally_ordered<typename std::iterator_traits<It>::value_type>
class Heap final {
public:
  using Self = Heap<It, Comp>;
  using SelfRef = Self&;
  using SelfConstRef = const Self&;
  using SelfRvalueRef = Self&&;

  using Iterator = It;
  using ItTraits = std::iterator_traits<It>;
  using Val = typename ItTraits::value_type;
  using ValRef = typename ItTraits::reference;
  using Diff = typename ItTraits::difference_type;
  using Size = std::size_t;

public:
  Heap() = delete;
  Heap(SelfConstRef) = delete;
  Heap(SelfRvalueRef) = default;
  SelfRef operator=(SelfConstRef) = delete;
  SelfRef operator=(SelfRvalueRef) = default;

  Heap(It first, It last)
      : m_store_begin{first}, m_store_end{last}, m_end{std::next(first)} {}

  bool insert(Val val) {
    if (is_full()) {
      return false;
    }
    *m_end = std::move(val);
    ++m_end;
    balance_up(std::prev(end()));
    return true;
  }
  bool replace(const Val& old_val, Val new_val) {
    const It ipos = find(begin(), old_val);
    if (ipos == end()) {
      return false;
    }
    *ipos = std::move(new_val);
    if (m_comp(*ipos, old_val)) {
      balance_down(ipos);
    } else {
      balance_up(ipos);
    }
    return true;
  }
  void pop() {
    if (is_empty()) {
      return;
    }
    *begin() = *std::prev(end());
    --m_end;
    balance_down(begin());
  }
  void clear() { m_end = begin(); }
  const Val& max() const { return *begin(); }
  inline bool is_full() const { return m_end == m_store_end; }
  inline bool is_empty() const { return begin() == m_end; }
  inline Size size() const { return std::distance(begin(), end()); }
  inline Size height() const {
    if (is_empty()) {
      return 0;
    }
    Size result{1};
    It last = std::prev(end());
    while (!is_root(last)) {
      ++result;
      last = parent(last);
    }
    return result;
  }

  inline It begin() { return m_store_begin + 1; }
  inline It end() { return m_end; }

  inline It left_child(const It node) const {
    return std::next(m_store_begin, std::distance(m_store_begin, node) * 2);
  }
  inline It right_child(const It node) const {
    return std::next(m_store_begin, std::distance(m_store_begin, node) * 2 + 1);
  }
  inline bool has_left_child(const It node) const {
    return left_child(node) < m_end;
  }
  inline bool has_right_child(const It node) const {
    return right_child(node) < m_end;
  }
  inline bool has_any_child(const It node) const {
    return has_left_child(node) || has_right_child(node);
  }

private:
  inline It parent(const It node) const {
    return std::next(m_store_begin, std::distance(m_store_begin, node) / 2);
  }
  inline It max_child(const It node) const {
    if (has_right_child(node)) {
      const It lc = left_child(node);
      const It rc = right_child(node);
      if (m_comp(*lc, *rc)) {
        return rc;
      }
    }
    return left_child(node);
  }
  inline bool is_root(const It node) const { return node == begin(); }
  It find(const It node, const Val& val) const {
    if (*node == val) {
      return node;
    } else if (!m_comp(val, *node)) {
      return end();
    }
    if (has_left_child(node)) {
      const It lres = find(left_child(node), val);
      if (lres != end()) {
        return lres;
      }
    }
    if (has_right_child(node)) {
      const It rres = find(right_child(node), val);
      if (rres != end()) {
        return rres;
      }
    }
    return end();
  }
  void balance_up(It node) {
    if (is_root(node)) {
      return;
    }
    const It p = parent(node);
    if (m_comp(*node, *p)) {
      return;
    }
    std::iter_swap(node, p);
    balance_up(p);
  }
  void balance_down(It node) {
    if (!has_any_child(node)) {
      return;
    }
    const It mc = max_child(node);
    if (m_comp(*mc, *node)) {
      return;
    }
    std::iter_swap(node, mc);
    balance_down(mc);
  }

  inline It begin() const { return m_store_begin + 1; }
  inline It end() const { return m_end; }

private:
  const It m_store_begin;
  const It m_store_end;
  It m_end;
  const Comp m_comp{};
};
} // namespace heap