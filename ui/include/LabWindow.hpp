#pragma once

#include <concepts>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <memory>
#include <qmainwindow.h>
#include <qobjectdefs.h>
#include <qspinbox.h>
#include <qwidget.h>
#include <random>
#include <utility>

#include "Heap.hpp"
#include "ui_LabWindow.h"

namespace ui {
class LabWindow : public QMainWindow {
  Q_OBJECT
public:
  using Val = std::int32_t;

  static constexpr std::size_t heap_capacity{16};
  static constexpr std::size_t random_size{15};
  static constexpr std::pair<Val, Val> random_limits{10, 99};

  using Self = LabWindow;
  using SelfRef = Self&;
  using SelfConstRef = const Self&;
  using SelfRvalueRef = Self&&;

  using Ui = Ui::LabWindow;
  using UiPtr = std::unique_ptr<Ui>;

  using Arr = std::array<Val, heap_capacity>;
  using ArrayPtr = std::unique_ptr<Arr>;
  using Heap = heap::Heap<typename Arr::iterator>;

  using RngDev = std::random_device;
  using Rng = std::mt19937_64;
  using RandDist = std::uniform_int_distribution<Val>;

public:
  LabWindow(SelfConstRef) = delete;
  LabWindow(SelfRvalueRef) = delete;
  SelfRef operator=(SelfConstRef) = delete;
  SelfRef operator=(SelfRvalueRef) = delete;

  explicit LabWindow(QWidget* = nullptr);

private:
  void configure_ui();
  void display_heap_array();
  void display_heap_tree();
  void display_subtree(const typename Heap::Iterator, const int, const int,
                       const int);

  template <std::integral T> void set_box_limit(QSpinBox* box) {
    box->setMaximum(std::numeric_limits<T>::max());
    box->setMinimum(std::numeric_limits<T>::min());
  }

private slots:
  void insert_in_heap();
  void clear_heap();
  void replace_in_heap();
  void pop_max_from_heap();
  void create_random_heap();

private:
  const UiPtr m_ui{std::make_unique<Ui>()};
  const ArrayPtr m_array{std::make_unique<Arr>()};
  Heap m_heap{m_array->begin(), m_array->end()};
  Rng m_gen{RngDev{}()};
};
} // namespace ui