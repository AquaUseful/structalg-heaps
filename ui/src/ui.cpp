#include <cstdlib>
#include <qapplication.h>

#include "LabWindow.hpp"

int main(int argc, char* argv[]) {
  QApplication app(argc, argv);
  ui::LabWindow lab{};
  lab.show();
  return app.exec();
}