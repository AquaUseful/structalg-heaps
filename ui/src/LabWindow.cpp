#include "LabWindow.hpp"

#include <QMessageBox>
#include <cstddef>
#include <functional>
#include <limits>
#include <qicon.h>
#include <qmainwindow.h>
#include <qmessagebox.h>
#include <qobjectdefs.h>
#include <qtablewidget.h>
#include <qwidget.h>

ui::LabWindow::LabWindow(QWidget* parent) : QMainWindow(parent) {
  m_ui->setupUi(this);
  m_ui->retranslateUi(this);
  configure_ui();
}

void ui::LabWindow::configure_ui() {
  set_box_limit<Val>(m_ui->insertBox);
  set_box_limit<Val>(m_ui->changeFromBox);
  set_box_limit<Val>(m_ui->changeToBox);

  connect(m_ui->insertBtn, SIGNAL(clicked()), this, SLOT(insert_in_heap()));
  connect(m_ui->clearBtn, SIGNAL(clicked()), this, SLOT(clear_heap()));
  connect(m_ui->changeBtn, SIGNAL(clicked()), this, SLOT(replace_in_heap()));
  connect(m_ui->popBtn, SIGNAL(clicked()), this, SLOT(pop_max_from_heap()));
  connect(m_ui->createBtn, SIGNAL(clicked()), this, SLOT(create_random_heap()));
}

void ui::LabWindow::display_heap_array() {
  m_ui->arrayList->clear();
  for (const auto& el : m_heap) {
    m_ui->arrayList->addItem(QString::number(el));
  }
}

void ui::LabWindow::display_subtree(const typename Heap::Iterator pos,
                                    const int row, const int col,
                                    const int gap) {
  QTableWidgetItem* item{new QTableWidgetItem{QString::number(*pos)}};
  m_ui->treeTable->setItem(row, col, item);
  if (m_heap.has_left_child(pos)) {
    display_subtree(m_heap.left_child(pos), row + 1, col - gap / 2, gap / 2);
  }
  if (m_heap.has_right_child(pos)) {
    display_subtree(m_heap.right_child(pos), row + 1, col + gap / 2, gap / 2);
  }
}

void ui::LabWindow::display_heap_tree() {
  m_ui->treeTable->clear();
  const int cols = (1 << m_heap.height()) - 1;
  m_ui->treeTable->setColumnCount(cols);
  m_ui->treeTable->setRowCount(m_heap.height());
  const int gap = 1 << (m_heap.height() - 1);
  display_subtree(m_heap.begin(), 0, m_ui->treeTable->columnCount() / 2, gap);
  m_ui->treeTable->resizeColumnsToContents();
  m_ui->treeTable->resizeColumnsToContents();
}

void ui::LabWindow::insert_in_heap() {
  if (m_heap.is_full()) {
    QMessageBox{"Невозможно вставить элемент", "Очередь заполнена",
                QMessageBox::Critical,         QMessageBox::Ok,
                QMessageBox::NoButton,         QMessageBox::NoButton}
        .exec();
  }
  const Val val = static_cast<Val>(m_ui->insertBox->value());
  m_heap.insert(val);
  display_heap_array();
  display_heap_tree();
}

void ui::LabWindow::clear_heap() {
  if (m_heap.is_empty()) {
    QMessageBox{"Нельзя очистить очередь", "Очередь уже пуста",
                QMessageBox::Warning,      QMessageBox::Ok,
                QMessageBox::NoButton,     QMessageBox::NoButton}
        .exec();
    return;
  }
  m_heap.clear();
  display_heap_array();
  display_heap_tree();
}

void ui::LabWindow::replace_in_heap() {
  const Val old_val = static_cast<Val>(m_ui->changeFromBox->value());
  const Val new_val = static_cast<Val>(m_ui->changeToBox->value());
  if (old_val == new_val) {
    QMessageBox{
        "Нет смысла заменять элемент", "Приоритет не изменится после замены",
        QMessageBox::Information,      QMessageBox::Ok,
        QMessageBox::NoButton,         QMessageBox::NoButton}
        .exec();
    return;
  }
  if (m_heap.replace(old_val, new_val)) {
    display_heap_array();
    display_heap_tree();
  } else {
    QMessageBox{"Невозможно заменить элемент",
                "Элемент с заменяемым приоритетом отсуствует",
                QMessageBox::Critical,
                QMessageBox::Ok,
                QMessageBox::NoButton,
                QMessageBox::NoButton}
        .exec();
  }
}

void ui::LabWindow::pop_max_from_heap() {
  if (m_heap.is_empty()) {
    QMessageBox{"Невозможно извлечь элемент", "Очередь пуста",
                QMessageBox::Critical,        QMessageBox::Ok,
                QMessageBox::NoButton,        QMessageBox::NoButton}
        .exec();
    return;
  }
  const Val max = m_heap.max();
  m_heap.pop();
  display_heap_array();
  display_heap_tree();
  m_ui->queryList->addItem(QString::number(max));
}

void ui::LabWindow::create_random_heap() {
  m_heap.clear();
  RandDist dist(random_limits.first, random_limits.second);
  for (std::size_t i{0}; i < random_size; ++i) {
    m_heap.insert(dist(m_gen));
  }
  display_heap_array();
  display_heap_tree();
}
